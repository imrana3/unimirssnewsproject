package com.imran3.unimirss;

import com.imran3.unimirss.adapter.NavDrawerListAdapter;
import com.imran3.unimirss.model.NavDrawerItem;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends FragmentActivity
{
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;

    private String urlDipart[],navScienze[];
    SharedPreferences prefsDip;
    private final String MIODIPART = "MIODIPART";
    private static int currentFrag = 0,mioDipart = 0;
    static String currentUrl = "http://www.ccdinf.unimi.it/it/avvisi.rss";
    FragmentManager fragmentManager;
	@Override
	protected void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();

		mTitle = mDrawerTitle = getTitle();
        urlDipart = getResources().getStringArray(R.array.url_dipart);//urls
        navScienze = getResources().getStringArray(R.array.lista_scienze);//titles
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);//icons
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        aggiornaNav();
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());//ascolta click lista

        try{//enabling action bar app icon and behaving it as toggle buttontton
		    getActionBar().setHomeButtonEnabled(true);
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(NullPointerException e){System.out.print(e.getMessage());}

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
            )
            {
                public void onDrawerClosed(View view)
                {
                    getActionBar().setTitle(mTitle);
                    invalidateOptionsMenu();
                }
                public void onDrawerOpened(View drawerView)
                {
                    getActionBar().setTitle(mDrawerTitle);
                    invalidateOptionsMenu();
                }
            };
        prefsDip = this.getSharedPreferences(MIODIPART, Context.MODE_PRIVATE);
        mioDipart = prefsDip.getInt("DIPSEL",-1);//prelevo indice del corso di laurea utente
        if(mioDipart == -1)//se non l'ha settato
            mioDipart=0;//visualizzo come default le NEWS
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		if (savedInstanceState == null)
        {
            adapter.selectItem(mioDipart);
			displayView(urlDipart[mioDipart],mioDipart);//item di default
		}
        //avvia il metodo app_launched dopo 10 secondi dall'avvio
        SharedPreferences prefs = MainActivity.this.getSharedPreferences("apprater", 0);
        if (!prefs.getBoolean("nonMostrarePiu", false))//avvia task solo se nonMostrarePiu è false
        {
            MyTimerTask taskShowRater = new MyTimerTask();
            Timer myTimer = new Timer();
            myTimer.schedule(taskShowRater, 10000);
        }
	}//fine metodo OnCreate()
    public void aggiornaNav()
    {
        navDrawerItems = new ArrayList<NavDrawerItem>();
        int indexImg = 0;
        for(String item : navScienze)
        {
            navDrawerItems.add(new NavDrawerItem(item, navMenuIcons.getResourceId(indexImg, 0)));
            indexImg++;
        }

        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);//create adapter
        mDrawerList.setAdapter(adapter);//set adapter fom menu
        adapter.selectItem(currentFrag);
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        aggiornaNav();//ripopolo il nav drawer
        mDrawerList.setItemChecked(currentFrag, true);//riseleziono quello attuale
        mDrawerList.setSelection(currentFrag);
        setTitle(navScienze[currentFrag]);
    }
    private class SlideMenuClickListener implements ListView.OnItemClickListener
    {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,long id)
        {
            adapter.selectItem(position);//setta il grassetto
            TextView textView = (TextView) view.findViewById(R.id.title);
            String itemText = (String)textView.getText();
            displayView(itemText, position);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
    {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
    {
		if (mDrawerToggle.onOptionsItemSelected(item))
			return true;
		switch (item.getItemId())
        {
            case R.id.action_settings:
                Intent intentSetting = new Intent(getBaseContext(), Impostazioni.class);
                startActivity(intentSetting);
                return true;
            case R.id.action_refresh:
                displayView(currentUrl,currentFrag);
                return true;
            default:
                return super.onOptionsItemSelected(item);
		}
	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
    {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        menu.findItem(R.id.action_refresh).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}
	private void displayView(String itemText,int position)
    {
        String url = "";
        for(int i = 0;i<navScienze.length;i++)
        {
            if(navScienze[i].equals(itemText))
                url = urlDipart[i];
        }
        if(url.equals(""))
            url = urlDipart[position];
		Fragment fragment = newInstance(url, position);
        currentFrag = position;
        currentUrl = urlDipart[position];
		fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commitAllowingStateLoss();
        // update selected item and title, then close the drawer
        setTitle(navScienze[position]);
        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
        mDrawerLayout.closeDrawer(mDrawerList);
	}
    //istanzia FragAvvisi e setta dei parametri (non si possono usare costruttore con paramentri
    //in fragments da android 4)
    public static final FragAvvisi newInstance(String url, int pos)
    {
        FragAvvisi fragment = new FragAvvisi();
        Bundle bundle = new Bundle(2);
        bundle.putInt("POS", pos);
        bundle.putString("URL", url);
        fragment.setArguments(bundle);
        return fragment ;
    }
	@Override
	public void setTitle(CharSequence title)
    {
		mTitle = title;
		getActionBar().setTitle(mTitle);
        getActionBar().setIcon(navMenuIcons.getResourceId(currentFrag, 0));
	}
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
    //timer task che avvia il metodo app_launched() per dialog mostra app.
    class MyTimerTask extends TimerTask {
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    VotaApp.app_launched(MainActivity.this);
                }
            });
        }
    }
}


