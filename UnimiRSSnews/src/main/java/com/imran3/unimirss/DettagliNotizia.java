package com.imran3.unimirss;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ShareActionProvider;
import android.widget.TableLayout;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;

public class DettagliNotizia extends Activity
{
    //Chiavi per xml parser
    static final String KEY_TITLE = "title";
    static final String KEY_CATEG = "category";
    static final String KEY_DATE = "pubDate";
    static final String KEY_LINK = "link";
    static final String KEY_DESC = "description";
    static String[] dipCritici, classNameCritici;
    String link ="", titolo = "", url = "", description = "";
    TableLayout tlTitolo;
    TextView tvTitolo, tvDesc;
    LinearLayout llProgress, llNews;
    private ShareActionProvider mShareActionProvider;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dettagli_notizia);

        try{// enabling action bar app icon and behaving it as toggle button
            ActionBar ab = getActionBar();
            if(ab!=null)
            {
                ab.setDisplayHomeAsUpEnabled(true);
                ab.setDisplayHomeAsUpEnabled(true);
            }
        }catch(NullPointerException e){
            Log.i("Errore", "" + e.getMessage());}
        llNews = (LinearLayout) findViewById(R.id.llDettagli);
        llProgress = (LinearLayout) findViewById(R.id.llProgress);
        dipCritici = getResources().getStringArray(R.array.critici);
        classNameCritici = getResources().getStringArray(R.array.class_name);

        // Prelevo valori dall'intent
        Intent in = getIntent();
        titolo = in.getStringExtra(KEY_TITLE);
        String data = in.getStringExtra(KEY_DATE);
        String categoria = in.getStringExtra(KEY_CATEG);
        description = in.getStringExtra(KEY_DESC);
        url = in.getStringExtra("DIP");
        link = in.getStringExtra(KEY_LINK);

        tlTitolo = (TableLayout)findViewById(R.id.tlTitolo);
        tvTitolo = (TextView) findViewById(R.id.tvTitolo);
        TextView tvData = (TextView) findViewById(R.id.tvData);
        TextView tvCategoria = (TextView) findViewById(R.id.tvCategorie);
        tvDesc = (TextView) findViewById(R.id.tvDescrizione);

        Button btnApriBrowser = (Button) findViewById(R.id.btnLink);
        btnApriBrowser.setHint("Per visualizzare gli allegati aprire la notizia nel browser.");
        btnApriBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(link);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        tvTitolo.setText(titolo);
        tvData.setText(data);

        if(!categoria.equals(""))
            tvCategoria.setText(categoria);
        else
        {
            tvCategoria.setVisibility(View.GONE);
            View vSeparatore = findViewById(R.id.vSeparatore);
            vSeparatore.setVisibility(View.GONE);
        }
        if(!description.equals(""))//se c'è una descrizione
        {
            int indexCritico = -1;
            for(int i = 0;i<dipCritici.length;i++)//verifico se devo caricare i dettagli a parte
                if(url.equals(dipCritici[i]))
                    indexCritico = i;
            if(indexCritico != -1)//se è Agraria, Beni cul e ambiet,Lettere, Filosofia o Storia
            {
               if(isOnline())
                {
                    HtmlParsingTask htmlParse = new HtmlParsingTask(this,classNameCritici[indexCritico]);
                    htmlParse.execute();
                }
                else
                {
                    tvDesc.setText("Impossibile caricare i dettagli. Assicurarsi di essere connessi a Internet");
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Nessuna connessione internet");
                    builder.setMessage("Impossibile caricare i dettagli. Assicurarsi di essere connessi a Internet.");
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
            else// se è uno di Scienze e tecnologie
                tvDesc.setText(description);
        }
    }

    public boolean isOnline()
    {
        ConnectivityManager CManager = (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo NInfo = CManager.getActiveNetworkInfo();
        return (NInfo != null && NInfo.isConnectedOrConnecting());
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //Crea il menu mettendoci il tasto di share
        getMenuInflater().inflate(R.menu.share_menu, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);
        mShareActionProvider = (ShareActionProvider) item.getActionProvider();
        setShareIntent();
        return true;
    }
    //Creo intent per la condivisione
    private void setShareIntent()
    {
        if (mShareActionProvider != null)
        {
            Intent intentCondividi = new Intent(Intent.ACTION_SEND);
            intentCondividi.setAction(Intent.ACTION_SEND);
            intentCondividi.setType("text/plain");
            intentCondividi.putExtra(Intent.EXTRA_TEXT, titolo + "\n" + link);
            mShareActionProvider.setShareIntent(intentCondividi);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();//ritorna a Activity precedente
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed()
    {
        this.finish();
        overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        //overridePendingTransition(R.anim.in_lista, R.anim.out_dettagli);
    }
    private class HtmlParsingTask extends AsyncTask<Void, Void, Boolean>
    {
        Context context;
        String className = "";
        public HtmlParsingTask(Context c,String className)
        {
            context = c;
            this.className = className;
        }
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            llNews.setVisibility(View.GONE);
            llProgress.setVisibility(View.VISIBLE);
        }
        @Override
        protected Boolean doInBackground(Void... voids)
        {

            Document doc = null;
            try
            {
                doc = Jsoup.connect(link).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try
            {
                Elements content = doc.getElementsByClass(className);
                Element p;
                for(int i=0; i<content.size(); i++)
                {
                    p = content.get(0);
                    description = p.text()+"\n";
                }
            }catch (NullPointerException e){
                description = "Si è verificato un errore di comunicazione con il sito dell'università!\n" +
                        "Riprova piu tardi.";}
            return true;
        }
        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result)
            {
                llNews.setVisibility(View.VISIBLE);
                llProgress.setVisibility(View.GONE);
                tvDesc.setText(description);
            }
        }
    }
}
