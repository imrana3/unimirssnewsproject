package com.imran3.unimirss;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class VotaApp
{
    private final static String APP_TITLE = "Unimi Avvisi";
    private final static String APP_PNAME = "com.imran3.unimirss";
    private final static int DAYS_UNTIL_PROMPT = 5;//giorni prima di mostrare dialog vota
    private static int LAUNCHES_UNTIL_PROMPT = 10;//avvii prima di mostrare dialog vota
    private static Dialog dialog;
    public static void app_launched(Context mContext)
    {
        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("nonMostrarePiu", false)) { return ; }
        SharedPreferences.Editor prefEditor = prefs.edit();
        // incrementa contatore avvii app
        long ctrAvvii = prefs.getLong("ctrAvvii", 0) + 1;
        prefEditor.putLong("ctrAvvii", ctrAvvii);
        // data primo avvio
        Long dataPrimoAvvio = prefs.getLong("dataPrimoAvvio", 0);
        if (dataPrimoAvvio == 0)
        {
            dataPrimoAvvio = System.currentTimeMillis();
            prefEditor.putLong("dataPrimoAvvio", dataPrimoAvvio);
        }
        // aspetta n giorni prima di aprire
        if (ctrAvvii >= LAUNCHES_UNTIL_PROMPT) 
            if (System.currentTimeMillis() >= dataPrimoAvvio + (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000))
                showRateDialog(mContext, prefEditor);
        prefEditor.commit();
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor prefEditor)
    {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f);
        //Contenitore per corpo dialogo
        LinearLayout llDialog = new LinearLayout(mContext);
        llDialog.setBackgroundResource(R.drawable.bg_about);
        llDialog.setOrientation(LinearLayout.VERTICAL);
        //contenitore bottoni
        LinearLayout llBottoni = new LinearLayout(mContext);
        llBottoni.setOrientation(LinearLayout.HORIZONTAL);
        //testo
        TextView tvTesto = new TextView(mContext);
        tvTesto.setText("Se ti piace " + APP_TITLE + ", supportaci votando l'app sul Play Store.");
        tvTesto.setLayoutParams(params);
        tvTesto.setPadding(5,5,5,5);
        llDialog.addView(tvTesto);
        //checkbox non mostrare piu dialog
        final CheckBox cbNonPiu = new CheckBox(mContext);
        cbNonPiu.setText("Non mostrare piu");
        cbNonPiu.setChecked(false);
        llDialog.addView(cbNonPiu);
        //bottone vota app
        Button btnVota = new Button(mContext);
        btnVota.setLayoutParams(params);
        btnVota.setText("Va bene");
        btnVota.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                dialog.dismiss();
                if (cbNonPiu.isChecked()) {
                    if (prefEditor != null) {
                        prefEditor.putBoolean("nonMostrarePiu", true);
                        prefEditor.commit();
                    }
                }
            }
        });
        //bottone non ora
       Button btnNonOra = new Button(mContext);
       btnNonOra.setLayoutParams(params);
       btnNonOra.setText("Non ora");
        btnNonOra.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
                if(cbNonPiu.isChecked())
                {
                    if (prefEditor != null) {
                        prefEditor.putBoolean("nonMostrarePiu", true);
                        prefEditor.commit();
                    }
                }
            }
        });
        //aggiungo bottoni al contenitore
        llBottoni.addView(btnNonOra);
        llBottoni.addView(btnVota);
        //aggiungo contenitore bottoni al contenitore corpo dialog
        llDialog.addView(llBottoni);
        dialog = new Dialog(mContext);
        dialog.setTitle("Vota "+ APP_TITLE);
        dialog.setContentView(llDialog);
        dialog.show();
    }
}