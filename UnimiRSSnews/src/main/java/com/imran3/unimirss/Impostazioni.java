package com.imran3.unimirss;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.imran3.unimirss.adapter.SettingAdapter;

public class Impostazioni extends Activity
{
    String[] scelte = {"Corso di laurea","Vota","Dillo a un amico","Contattaci","About",};
    private String app_link  ="https://play.google.com/store/apps/details?id=com.imran3.unimirss";
    Integer[] imageId = {R.drawable.set_dipart,
            R.drawable.rate,
            R.drawable.share_app,
            R.drawable.contact_us,
            R.drawable.about};
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.impostazioni);
        try{// enabling action bar app icon and behaving it as toggle button
            ActionBar ab = getActionBar();
            if(ab!=null)
            {
                ab.setDisplayHomeAsUpEnabled(true);
                ab.setDisplayHomeAsUpEnabled(true);
            }
        }catch(NullPointerException e){
            Log.i("Errore", "" + e.getMessage());}
        SettingAdapter adapter = new SettingAdapter(Impostazioni.this, scelte, imageId);
        list = (ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                switch(position)
                {
                    case 0://set corso laurea
                    {
                        Intent setDipIntent = new Intent(getBaseContext(), SetDipart.class);
                        startActivity(setDipIntent);
                        break;
                    }
                    case 1://vota
                    {
                        Impostazioni.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app_link)));
                        Toast.makeText(Impostazioni.this, "Grazie per il tuo feedback", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case 2://condividi app
                    {
                        Intent intentCondividi = new Intent(Intent.ACTION_SEND);
                        intentCondividi.setAction(Intent.ACTION_SEND);
                        intentCondividi.setType("text/plain");
                        intentCondividi.putExtra(Intent.EXTRA_TEXT, "Installa Unimi Avvisi per essere sempre aggiornato\n"+app_link);
                        startActivity(intentCondividi);
                        break;
                    }
                    case 3://contattaci
                    {
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","imranazam3@gmail.com", null));
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Unimi News");
                        startActivity(Intent.createChooser(emailIntent, "Invia Email..."));
                        break;
                    }
                    case 4://about
                    {
                        // Inflate the about message contents
                        View messageView = getLayoutInflater().inflate(R.layout.about, null, false);
                        if(messageView!=null)
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Impostazioni.this);
                            builder.setIcon(R.drawable.ic_launcher);
                            builder.setTitle(R.string.app_name);
                            builder.setView(messageView);
                            builder.create();
                            builder.show();
                            TextView tvAbout = (TextView) messageView.findViewById(R.id.tvAutore);
                            tvAbout.setMovementMethod(LinkMovementMethod.getInstance());
                            tvAbout.setText(Html.fromHtml("<b>Autore</b> : <a href='https://plus.google.com/+ImranAzam3'>Imran Azam</a>"));
                            TextView tvVersione = ((TextView)messageView.findViewById(R.id.tvVersione));
                            try {
                                tvVersione.setText(Html.fromHtml("<b>Versione</> : "+getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName));
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    }
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();//ritorna a Activity precedente
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
