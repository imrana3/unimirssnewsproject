package com.imran3.unimirss;

import android.app.AlertDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v4.app.ListFragment;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

public class FragAvvisi extends ListFragment
{
    static String url = "http://www.ccdinf.unimi.it/it/avvisi.rss";
    static String[] navScienze, mesi;
    static int position = 0;
    final String KEY_ITEM = "item", KEY_TITLE = "title", KEY_CATEG = "category";
    static final String KEY_GIORNO = "giorno", KEY_MESE = "mese", KEY_ANNO = "anno", KEY_DATE = "pubDate";
    static final String KEY_LINK = "link", KEY_DESC = "description";
    ArrayList<HashMap<String, String>> menuItems;
    View rootView;
    LinearLayout llProgress, llNews;
    XmlParsingTask parsingTask;
    public FragAvvisi(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.news_listview, container, false);
        mesi = getResources().getStringArray(R.array.mesi);//lista mesi
        navScienze = getResources().getStringArray(R.array.lista_scienze);//titles
        return rootView;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        url = getArguments().getString("URL");
        position = getArguments().getInt("POS");
        llNews = (LinearLayout)rootView.findViewById(R.id.llNews);
        llProgress = (LinearLayout)rootView.findViewById(R.id.llProgress);
        parsingTask = new XmlParsingTask();
        if(isOnline())//se c'è connessione prelevo dati
            parsingTask.execute();
        else//altrimenti segnalo errore
        {
            menuItems = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> errorMap = new HashMap<String, String>();
            Calendar oggi = Calendar.getInstance();
            errorMap.put(KEY_GIORNO,oggi.get(Calendar.DAY_OF_MONTH)+"");
            errorMap.put(KEY_MESE,mesi[oggi.get(Calendar.MONTH)]);
            errorMap.put(KEY_ANNO,oggi.get(Calendar.YEAR)+"");
            errorMap.put(KEY_TITLE, "Nessuna connessione Internet.");
            errorMap.put(KEY_CATEG,"Assicurarsi di essere connessi a Internet");
            menuItems.add(errorMap);
            ListAdapter adapter = new SimpleAdapter(getActivity(), menuItems,
                    R.layout.news_list_item,
                    new String[] { KEY_GIORNO,KEY_MESE,KEY_ANNO, KEY_TITLE, KEY_CATEG}, new int[]
                    {R.id.tvGiorno,R.id.tvMese,R.id.tvAnno,R.id.tvTitolo,R.id.tvCategorie});
            setListAdapter(adapter);
        }
        ListView lv = getListView();
        if(lv!=null)
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,int position, long id)
                {
                    if(isOnline())
                    {
                        HashMap<String, String> map = menuItems.get(position);
                        if(!map.get(KEY_TITLE).equals("Nessuna connessione Internet."))
                        {
                            String data = map.get(KEY_GIORNO)+" "+map.get(KEY_MESE)+" "+map.get(KEY_ANNO);
                            map.get(KEY_TITLE);
                            Intent inDettagli = new Intent(getActivity(), DettagliNotizia.class);
                            inDettagli.putExtra(KEY_TITLE, map.get(KEY_TITLE));
                            inDettagli.putExtra(KEY_DATE, data);
                            inDettagli.putExtra(KEY_CATEG, map.get(KEY_CATEG));
                            inDettagli.putExtra(KEY_DESC, map.get(KEY_DESC));
                            inDettagli.putExtra(KEY_LINK, map.get(KEY_LINK));
                            inDettagli.putExtra("DIP",url);
                            startActivity(inDettagli);
                            getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                        }
                        else
                            parsingTask.execute();
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Nessuna connessione internet");
                        builder.setMessage("Impossibile aprire dettagli. Assicurarsi di essere connessi a Internet.");
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            });
    }
    public boolean isOnline()
    {
        ConnectivityManager CManager = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo NInfo = CManager.getActiveNetworkInfo();
        return (NInfo != null && NInfo.isConnectedOrConnecting());
    }
    private class XmlParsingTask extends AsyncTask<Void, Void, Boolean>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            if(llNews!=null){
            llNews.setVisibility(View.GONE);
            llProgress.setVisibility(View.VISIBLE);}
            int currentOrientation = getResources().getConfiguration().orientation;
            if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE)
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            else
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
        @Override
        protected Boolean doInBackground(Void... params)
        {
            try
            {
                menuItems = new ArrayList<HashMap<String, String>>();
                XMLParser parser = new XMLParser();
                String xml = parser.getXmlFromUrl(url); // getting XML
                Document doc = parser.getDomElement(xml); // getting DOM element
                NodeList nlItem = doc.getElementsByTagName(KEY_ITEM);
                if(nlItem!=null)
                {
                    NodeList nlCateg;
                    String categorie="",giorno="",desc;
                    for (int i = 0; i < nlItem.getLength(); i++)
                    {
                        HashMap<String, String> map = new HashMap<String, String>();
                        Element e = (Element) nlItem.item(i);
                        if(e!=null)
                        {
                            String data = parser.getValue(e, KEY_DATE);
                            switch(position)
                            {
                                case 0:
                                case 1:
                                case 2:
                                case 16:
                                {
                                    data = data.substring(4,16);
                                    StringTokenizer token = new StringTokenizer(data," ");
                                    if(token.countTokens()==3)
                                    {
                                        giorno = token.nextToken();
                                        map.put(KEY_MESE,(token.nextToken()).toUpperCase(Locale.getDefault()));
                                        map.put(KEY_ANNO,token.nextToken());
                                    }
                                    NodeList title = e.getElementsByTagName(KEY_DESC);
                                    Element line = (Element) title.item(0);
                                    desc = parser.getCharacterDataFromElement(line);
                                    if(position == 16)
                                        desc = "N/A";
                                    break;
                                }
                                case 10:
                                {
                                    StringTokenizer token = new StringTokenizer(data,"-");
                                    map.put(KEY_ANNO,token.nextToken());
                                    map.put(KEY_MESE,(mesi[Integer.parseInt(token.nextToken())-1]));
                                    giorno = token.nextToken();
                                    if(giorno.length()>2)
                                        giorno = giorno.substring(0,2);
                                    desc = parser.getValue(e,KEY_DESC);
                                    break;
                                }
                                case 13:
                                case 14:
                                case 15:
                                case 17:
                                {
                                    data = data.substring(5);
                                    StringTokenizer token = new StringTokenizer(data," ");
                                    giorno = token.nextToken();
                                    if(giorno.length()>2)
                                        giorno = giorno.substring(0,2);
                                    map.put(KEY_MESE,(token.nextToken().substring(0,3).toUpperCase()));
                                    map.put(KEY_ANNO,token.nextToken());
                                    desc = parser.getValue(e,KEY_DESC);
                                    break;
                                }
                                default:
                                {
                                    StringTokenizer token = new StringTokenizer(data,"-");
                                    map.put(KEY_ANNO,token.nextToken());
                                    map.put(KEY_MESE,(mesi[Integer.parseInt(token.nextToken())-1]));
                                    giorno = token.nextToken();
                                    if(giorno.length()>2)
                                        giorno = giorno.substring(0,2);

                                    nlCateg = e.getElementsByTagName(KEY_CATEG);
                                    for(int j=0;j < nlCateg.getLength();j++)
                                        categorie += nlCateg.item(j).getTextContent()+" ";
                                    desc = parser.getValue(e,KEY_DESC);
                                    break;
                                }
                            }
                            if(!parser.getValue(e, KEY_TITLE).equals(""))//solo se ha un titolo valido
                            {
                                map.put(KEY_DATE, data);
                                map.put(KEY_GIORNO, giorno);
                                map.put(KEY_TITLE, parser.getValue(e, KEY_TITLE));
                                map.put(KEY_CATEG, categorie.toLowerCase());
                                map.put(KEY_LINK, parser.getValue(e, KEY_LINK));
                                map.put(KEY_DESC, pulisciDesc(desc));
                                menuItems.add(map);
                            }
                            categorie = "";
                        }
                    }
                }
                return true;
            } catch(Exception e){
                e.printStackTrace();
                return false;
            }
        }
        @Override
        protected void onPostExecute(Boolean result)
        {
            if(result)
            {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                ListAdapter adapter = new SimpleAdapter(getActivity(), menuItems,
                        R.layout.news_list_item,
                        new String[] { KEY_GIORNO,KEY_MESE,KEY_ANNO, KEY_TITLE, KEY_CATEG}, new int[]
                        {R.id.tvGiorno,R.id.tvMese,R.id.tvAnno,R.id.tvTitolo,R.id.tvCategorie});
                setListAdapter(adapter);
                if(llNews!=null){
                llNews.setVisibility(View.VISIBLE);
                llProgress.setVisibility(View.GONE);}
            }
        }
        public String pulisciDesc(String desc)
        {
            String oldS[] = {"<br>","<br/>","<em>","</em>"};
            for(String s : oldS)
            {
                desc = desc.replace(s,"");
                desc = desc.replace(s.toUpperCase(),"");
            }
            return desc;
        }
    }
}
