package com.imran3.unimirss;

import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class SetDipart extends ListActivity
{
    private final String MIODIPART = "MIODIPART";
    String listaDipart[];
    SharedPreferences prefsDip;
    int sel = 0;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dipart_set);
        try{// enabling action bar app icon and behaving it as toggle button
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(NullPointerException e){}
        prefsDip = this.getSharedPreferences(MIODIPART, Context.MODE_PRIVATE);
        sel = prefsDip.getInt("DIPSEL",-1);
        if(sel == -1)
            sel = 0;
        listaDipart =getResources().getStringArray(R.array.lista_scienze);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice,listaDipart));
        final ListView lvDipart = getListView();
        lvDipart.setItemsCanFocus(false);
        lvDipart.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvDipart.setItemChecked(sel,true);
        lvDipart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,int position, long id)
            {
                sel = position;
            }
        });
        Button btnSalva = (Button) findViewById(R.id.btnSalvaDip);
        btnSalva.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                prefsDip.edit().remove("DIPSEL");
                prefsDip.edit().putInt("DIPSEL", sel).commit();
                onBackPressed();
                Toast.makeText(getApplicationContext(), "Corso di laurea impostato", Toast.LENGTH_SHORT).show();
            }
        });
        Button btnAnnulla = (Button) findViewById(R.id.btnAnnulla);
        btnAnnulla.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onBackPressed();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();//ritorna a Activity precedente
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

