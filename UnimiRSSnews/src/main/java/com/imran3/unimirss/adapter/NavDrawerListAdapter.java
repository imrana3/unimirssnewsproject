package com.imran3.unimirss.adapter;

import com.imran3.unimirss.R;
import com.imran3.unimirss.model.NavDrawerItem;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawerListAdapter extends BaseAdapter
{
	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
    private int lastPosition = -1;
    private int selectedItem;

    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems)
    {
	    this.context = context;
		this.navDrawerItems = navDrawerItems;
	}
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        TextView txtCount = (TextView) convertView.findViewById(R.id.counter);
        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
        txtTitle.setText(navDrawerItems.get(position).getTitle());
        if(navDrawerItems.get(position).getCounterVisibility())
            txtCount.setText(navDrawerItems.get(position).getCount());
        else
            txtCount.setVisibility(View.GONE);
        //animazione caduta elementi nav drawer
        Animation animation = AnimationUtils.loadAnimation(convertView.getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;
        //grassetto e cambia colore item selezionato
        //txtTitle.setTypeface(null, position == selectedItem ? Typeface.BOLD : Typeface.NORMAL);
        if(position == selectedItem)
            txtTitle.setTextColor(Color.WHITE);
        else
            txtTitle.setTextColor(Color.parseColor("#5E5E5E"));
        return convertView;
    }
	@Override
	public int getCount() {
		return navDrawerItems.size();
	}
	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
    public void selectItem(int selectedItem){
        this.selectedItem = selectedItem;
        notifyDataSetChanged();
    }
}
